#!/usr/bin/python

##
## This script reads Nessus XML and updates Confluence space
##

import report
import confluence
import config
import argparse
import os
import pdb

parser = argparse.ArgumentParser()
parser.add_argument('--nessus_xml_file')
args = parser.parse_args()
if args.nessus_xml_file is None:
  raise Exception("Please provide a nessus xml file")
else:
  if os.path.isabs(args.nessus_xml_file):
    nessus_xml_file = args.nessus_xml_file
  else:
    nessus_xml_file = os.path.realpath(args.nessus_xml_file)

report = report.Report(nessus_xml_file)

confluence.update_findings(report.info_severity_findings, config.INFO_SEVERITY_TITLE)
confluence.update_findings(report.low_severity_findings, config.LOW_SEVERITY_TITLE)
confluence.update_findings(report.medium_severity_findings, config.MEDIUM_SEVERITY_TITLE)
confluence.update_findings(report.high_severity_findings, config.HIGH_SEVERITY_TITLE)
confluence.update_findings(report.critical_severity_findings, config.CRITICAL_SEVERITY_TITLE)

confluence.update_hosts(report)

confluence.update_scan_log(report)