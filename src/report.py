import xml
import xml.etree.ElementTree
import pdb
from itertools import groupby

class Finding(object):
  def __init__(self, **kw):
    self.__dict__ = kw
    self.wiki_page_id = None
    self.wiki_page_url = None
    self.version = None
    self.details_str = None

class Report(object):
  severities = [0, 1, 2, 3, 4]

  def __init__(self, nessus_xml_file):
    self.nessus_xml_file = nessus_xml_file

    self.info_severity_findings = list()
    self.low_severity_findings = list()
    self.medium_severity_findings = list()
    self.high_severity_findings = list()
    self.critical_severity_findings = list()

    self.nessus_xml_tree = xml.etree.ElementTree.parse(nessus_xml_file)
    self.hosts = {}
    self.scanlog = None
    self.findings = []

    for x in self.nessus_xml_tree.getroot().find("Report").findall("ReportHost"):
      host_url = x.attrib["name"]
      self.hosts[host_url] = {"id_list": [], "properties": {}, "wiki_page_id": None, "wiki_page_url": None, "version": None}

      for y in x.findall("ReportItem"):
        dts_tag = y.find("plugin_output")
        dts = ""

        if dts_tag != None:
          dts = dts_tag.text
          
        # todo refactor
        cve = list(map(lambda x: x.text, y.findall("cve")))
        see_also_tag = y.find("see_also")
        see_also = see_also_tag.text if see_also_tag != None else None

        exploit_available_tag = y.find("exploit_available")
        exploit_available = exploit_available_tag.text if exploit_available_tag != None else None

        port_tag = y.find("port")
        port = port_tag.text if port_tag != None else None

        f = Finding(
          id_=int(y.attrib["pluginID"]), 
          title=y.attrib["pluginName"], 
          impact=y.find("synopsis").text, 
          affected_hosts=host_url, 
          recommendations=y.find("solution").text, 
          severity=int(y.attrib["severity"]), 
          description=y.find("description").text, 
          details=[{"port": y.attrib["port"], "host": host_url, "plugin_output": dts}],
          cve=", ".join(cve),
          see_also=see_also,
          exploit_available=exploit_available,
          ports=[y.attrib["port"]])
        
        self.hosts[host_url]["id_list"].append(f.id_)
        if y.attrib["pluginName"] == "Nessus Scan Information":
          if self.scanlog == None:
            self.scanlog = {}
            self.scanlog["details"] = dts 
        else:
          if f.severity not in self.severities:
            raise Exception("Unknow type of severity: {}".format(f.severity))
          
          # todo - generate title properly
          fnd = list(filter(lambda x: x.title == f.title, self.critical_severity_findings))
          if len(fnd) > 0:
            f.title += " ({})".format(str(len(fnd) + 1))
          
          # merge with the same id
          fnd2 = list(filter(lambda x: x.id_ == f.id_, self.findings))
          if len(fnd2) > 0:
            fnd2[0].details.append({"port": y.attrib["port"], "host": host_url, "plugin_output": dts})
          else:
            self.findings.append(f)

      for y1 in x.find("HostProperties").findall("tag"):
        nm = y1.attrib["name"]
        if nm == "HOST_START":
          self.scanlog["start_date"] = " ".join(y1.text.split()) 

    for x in self.findings:
      dt = map(lambda x: "<b>port: </b>{}<br /><b>host: </b><![CDATA[{}]]><br /><b>plugin_output: </b><![CDATA[{}]]><br /><br />".
        format(x["port"], x["host"], x["plugin_output"].replace("<", "&lt;").replace(">", "&gt;").replace("]]>", "]]&gt")), x.details)
      
      x.details_str = "<br />".join(dt)

    self.info_severity_findings = list(filter(lambda x: x.severity == 0, self.findings))
    self.low_severity_findings = list(filter(lambda x: x.severity == 1, self.findings))
    self.medium_severity_findings = list(filter(lambda x: x.severity == 2, self.findings))
    self.high_severity_findings = list(filter(lambda x: x.severity == 3, self.findings))
    self.critical_severity_findings = list(filter(lambda x: x.severity == 4, self.findings))