# http://stackoverflow.com/questions/14585365/how-do-i-create-text-file-for-confluence-and-then-import-it-as-a-confluence-wiki

SITE_URL = "https://n2ctest2.atlassian.net"
SPACE_NAME = "INFRASCAN"
USERNAME = "admin"
PASSWORD = "qwerty123456"

INFO_SEVERITY_TITLE = "Info Severity"
LOW_SEVERITY_TITLE = "Low Severity"
MEDIUM_SEVERITY_TITLE = "Medium Severity"
HIGH_SEVERITY_TITLE = "High Severity"
CRITICAL_SEVERITY_TITLE = "Critical Severity"