#!/usr/bin/python

import config
import requests
import json
import pdb
import time

HTTP_MIN_ERROR_STATUS = 300
API_CONTENT_URL = config.SITE_URL + "/wiki/rest/api/content"
API_CONTENT_URL2 = config.SITE_URL + "/confluence/rest/api/content"
BODY_TEMPLATE = """
<h2>Plugin ID</h2>
{id_}
<br />

<h2>Description</h2>
<pre>{description}</pre>
<br />

<h2>Affected systems</h2>
{affected_hosts}
<br />

<h2>Recommendations</h2>
{recommendations}
<br />

<h2>Details</h2>
<pre>{details_str}</pre>
<br />

<h2>Cve</h2>
{cve}
<br />

<h2>See also</h2>
{see_also}
<br />

<h2>Exploit available</h2>
{exploit_available}
""".strip()

AUTH = (config.USERNAME, config.PASSWORD)
HEADERS = ({"Content-Type": "application/json", "Accept": "application/json"})

def build_wiki_page_data_obj(x):
  return {"id": x["id"], "url": "{}/wiki{}".format(config.SITE_URL, x["_links"]["webui"]), "title": x["title"].lower(), "version": x["version"]["number"]}

# todo - refactor
# findings
def update_findings(findings, parent_page_title):
  print("Updating findings for '{}' ...".format(parent_page_title))
  pp_id = get_wiki_page_id_by_title(parent_page_title)
  if pp_id == -1:
    print("The parent page '{}' doesn't exist".format(parent_page_title))
    return

  r = requests.get(API_CONTENT_URL2 + "/search?cql=parent={}&expand=version&limit=100".format(pp_id), params={}, auth=AUTH)
  wiki_pages = list(map(build_wiki_page_data_obj, r.json()["results"]))
  for x1 in wiki_pages:
    for y1 in findings:
      if x1["title"] == y1.title.lower():
        y1.wiki_page_id = x1["id"]
        y1.wiki_page_url = x1["url"]
        y1.version = x1["version"]

  wiki_page_titles = list(map(lambda x: x["title"].lower(), r.json()["results"]))
  findings_to_insert = list(filter(lambda x: x.title.lower() not in wiki_page_titles, findings))
  findings_to_update = list(filter(lambda x: x.title.lower() in wiki_page_titles, findings))

  finding_ids = list(map(lambda x: x.wiki_page_id, findings))
  wiki_page_ids = list(map(lambda x: x["id"], r.json()["results"]))
  wiki_page_ids_to_delete = list(filter(lambda x: x not in finding_ids, wiki_page_ids))

  # inserting
  i1 = 0
  print("{} findings of severity {} will be inserted".format(len(findings_to_insert), parent_page_title))
  for x in findings_to_insert:
    body = BODY_TEMPLATE.format(**vars(x))
    page_data = {"type": "page", "title": x.title, "ancestors": [{"id": pp_id}], "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}}

    r1 = requests.post(API_CONTENT_URL, data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    i1 += 1

    if r1.status_code >= HTTP_MIN_ERROR_STATUS:
      print("[{}][{}] inserted '{}' ({}) \n".format(i1, r1.status_code, x.title, r1.text))
    else:
      print("[{}][{} OK] inserted '{}'".format(i1, r1.status_code, x.title))

  print("Total inserted findings: {} \r\n".format(i1))


  time.sleep(5)
  r2 = requests.get(API_CONTENT_URL2 + "/search?cql=parent={}&expand=version&limit=100".format(pp_id), params={}, auth=AUTH)
  wiki_pages2 = list(map(build_wiki_page_data_obj, r2.json()["results"]))
  for x1 in wiki_pages2:
    for y1 in findings:
      if x1["title"] == y1.title.lower():
        y1.wiki_page_id = x1["id"]
        y1.wiki_page_url = x1["url"]
        y1.version = x1["version"]

  # updating
  i2 = 0
  print("{} findings of severity {} will be updated".format(len(findings_to_update), parent_page_title))
  for x in findings_to_update:
    body = BODY_TEMPLATE.format(**vars(x))
    new_version = x.version + 1
    page_data = {"id": x.wiki_page_id, "ancestors": [{"id": pp_id}], "type": "page", "title": x.title, "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}, "version": {"number": new_version}}

    r2 = requests.put("{}/{}".format(API_CONTENT_URL, x.wiki_page_id), data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    i2 += 1
    
    if r2.status_code >= HTTP_MIN_ERROR_STATUS:
      print("[{}][{}] updated '{}' ({})\n".format(i2, r2.status_code, x.title, r2.text))
    else:
      print("[{}][{} OK] updated '{}', version: {}".format(i2, r2.status_code, x.title, new_version))
   

  print("Total updated findings: {} \r\n".format(i2))

  # deleting
  i3 = 0
  print("{} findings of severity {} will be deleted".format(len(wiki_page_ids_to_delete), parent_page_title))
  for x in wiki_page_ids_to_delete:
    r3 = requests.delete(API_CONTENT_URL + "/{}".format(x), auth=AUTH, headers=HEADERS)
    i3 += 1
    print("[{}] deleted, id: {} [{} - {}]\n".format(i3, x, r3.status_code, r3.text))

  print("Total deleted findings: {} \r\n".format(i3))
  print("Done with findings for {}\r\n".format(parent_page_title))

# hosts
def update_hosts(report):
  print("Updating hosts...")
  pp_id = get_wiki_page_id_by_title("Findings by Host")
  if pp_id == -1:
    print("The parent page '{}' doesn't exist".format(parent_page_title))
    return

  r = requests.get(API_CONTENT_URL2 + "/search?cql=parent={}&expand=version&limit=100".format(pp_id), params={}, auth=AUTH)
  wiki_json_res = r.json()["results"]
  # todo - check if []

  for wiki_pages_item in map(build_wiki_page_data_obj, wiki_json_res):
    for nessus_items_item in report.hosts.keys():
      if wiki_pages_item["title"] == nessus_items_item.lower():
        report.hosts[nessus_items_item]["wiki_page_id"] = wiki_pages_item["id"]
        report.hosts[nessus_items_item]["wiki_page_url"] = wiki_pages_item["url"]
        report.hosts[nessus_items_item]["version"] = wiki_pages_item["version"]

  wiki_page_titles = list(map(lambda x: x["title"].lower(), wiki_json_res))
  nessus_items_to_insert = list(filter(lambda x: x.lower() not in wiki_page_titles, report.hosts.keys()))
  nessus_items_to_update = list(filter(lambda x: x.lower() in wiki_page_titles, report.hosts.keys()))

  nessus_ids = list(map(lambda x: report.hosts[x]["wiki_page_id"], report.hosts))
  wiki_page_ids = list(map(lambda x: x["id"], r.json()["results"]))
  wiki_item_ids_to_delete = list(filter(lambda x: x not in nessus_ids, wiki_page_ids))
  
  # inserting
  i = 0
  print("{} hosts will be inserted".format(len(nessus_items_to_insert)))
  for x in nessus_items_to_insert:
    nessus_items = list(filter(lambda x1: x1.id_ in report.hosts[x]["id_list"], report.findings))
    titles = list(map(lambda x1: "<a href='{}'><![CDATA[{}]]></a>".format(x1.wiki_page_url, x1.title), nessus_items))
    body = "<br />".join(titles)
    page_data = {"type": "page", "title": x, "ancestors": [{"id": pp_id}], "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}}

    r1 = requests.post(API_CONTENT_URL, data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    i += 1
    print("[{}] inserted, title: '{}', status: {}".format(i, x, r1.status_code))

  print("Total inserted: {} \r\n".format(i))

  # updating
  i2 = 0
  print("{} hosts will be updated".format(len(nessus_items_to_update)))
  for x in nessus_items_to_update:
    nessus_items = list(filter(lambda x1: x1.id_ in report.hosts[x]["id_list"], report.findings))
    titles = list(map(lambda x1: "<a href='{}'><![CDATA[{}]]></a>".format(x1.wiki_page_url, x1.title), nessus_items))
    body = "<br />".join(titles)
    new_version = report.hosts[x]["version"] + 1
    page_data = {"id": report.hosts[x]["wiki_page_id"], "ancestors": [{"id": pp_id}], "type": "page", "title": x, "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}, "version": {"number": new_version}}

    r2 = requests.put("{}/{}".format(API_CONTENT_URL, report.hosts[x]["wiki_page_id"]), data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    i2 += 1
    print("[{}] updated, title: '{}', version: {}, status: {}".format(i2, x, new_version, r2.status_code))

  print("Total updated: {} \r\n".format(i2))

  # deleting
  i3 = 0
  print("{} hosts will be deleted".format(len(wiki_item_ids_to_delete)))
  for x in wiki_item_ids_to_delete:
    r1 = requests.delete(API_CONTENT_URL + "/{}".format(x), auth=AUTH, headers=HEADERS)
    i3 += 1
    print("[{}] deleted, id: {}, status: {}".format(i3, x, r1.status_code))

  print("Total deleted: {} \r\n".format(i3))

# scan log
def update_scan_log(report):
  pp_id = get_wiki_page_id_by_title("Scan Log")
  r = requests.get(API_CONTENT_URL2 + "/search?cql=parent={}&expand=version&limit=100".format(pp_id), params={}, auth=AUTH)
  
  wiki_pages = list(map(build_wiki_page_data_obj, r.json()["results"]))
  wiki_page_id = None
  version = None
  title = report.scanlog["start_date"]
  wiki_page_obj = list(filter(lambda x1: x1["title"] == title.lower(), wiki_pages))
  body = "<pre>{}</pre>".format(report.scanlog["details"])
  if len(wiki_page_obj) == 0:
    # insert
    print("1 scanlog will be inserted")
    page_data = {"type": "page", "title": title, "ancestors": [{"id": pp_id}], "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}}

    r1 = requests.post(API_CONTENT_URL, data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    print("[1] inserted scanlogs, title: '{}', status: {}".format(title, r1.status_code))
    print("Total inserted scanlogs: 1\r\n")
  else:
    # update
    print("1 scanlog will be updated")
    new_version = wiki_page_obj[0]["version"] + 1
    page_data = {"id": wiki_page_obj[0]["id"], "ancestors": [{"id": pp_id}], "type": "page", "title": title, "space": {"key": config.SPACE_NAME},"body":{"storage": {
      "value": body, "representation": "storage"}}, "version": {"number": new_version}}

    r1 = requests.put("{}/{}".format(API_CONTENT_URL, wiki_page_obj[0]["id"]), data=json.dumps(page_data), auth=AUTH, headers=HEADERS)
    print("[1] updated scanlog(s), title: '{}', version: {}, status: {}".format(title, new_version, r1.status_code))
    print("Total updated scanlogs: 1\r\n")

  print("Done with scanlogs")

def print_response(r):
  print("{} {}\n".format(json.dumps(r.json(), sort_keys=True, indent=4, separators=(",", ": ")), r))

def get_wiki_page_id_by_title(title):
  r1 = requests.get(API_CONTENT_URL, params={"title": title}, auth=(config.USERNAME, config.PASSWORD))
  res = -1
  if len(r1.json()["results"]) > 0:
    res = int(r1.json()["results"][0]["id"])

  return res