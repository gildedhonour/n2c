One scan:

 * Test_2310_9xyy05.nessus - XML-formatted
 * Test_2310_iiofvj.html, Test_2310_xl7yat.html, Test_2310_zeh6q1.html - HTML-formatted, probably unnecessary, just in case

Another scan, with one finding (about TFTP on kali-abb) gone:

 * Test2347_klc0k7.nessus - XML

Sample Confluence space, manually created by me:

 * Confluence-space-export-002842-21.xml.zip - XML, you might be able to re-import it into your instance of Confluence
 * Confluence-space-export-002740-19.html.zip - HTML, probably unnecessary, just in case

Note I have added comments to some pages to hopefully clarify things.

